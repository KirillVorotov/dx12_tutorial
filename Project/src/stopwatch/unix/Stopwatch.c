#include "config.h"

#if defined(PLATFORM_UNIX) || defined(PLATFORM_LINUX)

#include <stdbool.h>
#include <sys/time.h>
#include "Stopwatch.h"

#define HAVE_CLOCK_GETTIME 1

#if HAVE_CLOCK_GETTIME
#include <time.h>
#endif

#if HAVE_CLOCK_GETTIME
	#ifdef CLOCK_MONOTONIC_RAW
		#define STOPWATCH_MONOTONIC_CLOCK CLOCK_MONOTONIC_RAW
	#else
		#define STOPWATCH_MONOTONIC_CLOCK CLOCK_MONOTONIC
	#endif
#endif

#if HAVE_CLOCK_GETTIME
static struct timespec start_ts;
#endif
static bool has_monotonic_time = false;
static struct timeval start_tv;
static bool started = false;

void Stopwatch_Init(void)
{
	if (started)
		return;
	started = true;

#if HAVE_CLOCK_GETTIME
	if (clock_gettime(STOPWATCH_MONOTONIC_CLOCK, &start_ts) == 0)
	{
		has_monotonic_time = true;
	}
	else
#endif
	{
		gettimeofday(&start_tv, NULL);
	}
}

void Stopwatch_Quit(void)
{
	started = false;
}

double Stopwatch_StartTimeMicroseconds(void)
{
	if (!started)
	{
		Stopwatch_Init();
	}

#if HAVE_CLOCK_GETTIME
	return start_ts.tv_sec * 1000000.0 + start_ts.tv_nsec / 1000.0;
#elif
	return start_tv.tv_sec * 1000000.0 + start_tv.tv_usec;
#endif
}

double Stopwatch_TimeMicroseconds(void)
{
	if (!started)
	{
		Stopwatch_Init();
	}

	if (has_monotonic_time)
	{
#if HAVE_CLOCK_GETTIME
		struct timespec now;
		clock_gettime(STOPWATCH_MONOTONIC_CLOCK, &now);
		return (now.tv_sec - start_ts.tv_sec) * 1000000.0 + (now.tv_nsec - start_ts.tv_nsec) / 1000.0;
#endif
	}
	else
	{
		struct timeval now;
		gettimeofday(&now, NULL);
		return (now.tv_sec - start_tv.tv_sec) * 1000000.0 + (now.tv_usec - start_tv.tv_usec);
	}
}

#endif