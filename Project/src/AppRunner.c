#include <stdio.h>
#include "Stopwatch.h"
#include "AppRunner.h"

static void FixedUpdate(double t, double dt)
{
	
}

static void Update(double t, double dt)
{

}

int Application_Init(void)
{
	Stopwatch_Init();
	realtimeSinceStartup = Stopwatch_TimeMicroseconds() / 1000.0;
	maxFrameTime = 250.0;
	deltaTime = 8.333333333; // 120 Frames per second max
	timeAccumulator = 0.0;
	fixedTime = realtimeSinceStartup;
	fixedDeltaTime = 20.0;
	fixedTimeAccumulator = 0.0;

	running = true;
	initialized = true;

	return 0;
}

int Application_Run(int (*update_callback)(double, double), int (*fixed_update_callback)(double, double))
{
	if (!initialized)
		Application_Init();

	/*Main loop*/
	while (running)
	{
		double newTime = Stopwatch_TimeMicroseconds() / 1000.0;
		double frameTime = newTime - realtimeSinceStartup;
		if (frameTime > maxFrameTime)
			frameTime = maxFrameTime;
		realtimeSinceStartup = newTime;

		/*Fixed Update*/
		fixedTimeAccumulator += frameTime;
		while (fixedTimeAccumulator >= fixedDeltaTime)
		{
			fixedTime += fixedDeltaTime;
			FixedUpdate(fixedTime, fixedDeltaTime);
			if (fixed_update_callback != NULL)
				fixed_update_callback(fixedTime, fixedDeltaTime);
			fixedTimeAccumulator -= fixedDeltaTime;
		}

		/*Update*/
		timeAccumulator += frameTime;
		if (timeAccumulator >= deltaTime)
		{
			Update(realtimeSinceStartup, timeAccumulator);
			if (update_callback != NULL)
				update_callback(realtimeSinceStartup, timeAccumulator);
			timeAccumulator = 0.0;
		}
	}

	return 0;
}

void Application_Stop(void)
{
	running = false;
}

double Application_RealtimeSinceStartup(void)
{
	return realtimeSinceStartup;
}

double Application_DeltaTime(void)
{
	return deltaTime;
}

double Application_FixedTimeSinceStartup(void)
{
	return fixedTime;
}

double Application_FixedDeltaTime(void)
{
	return fixedDeltaTime;
}