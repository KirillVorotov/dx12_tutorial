#ifndef Point2_h
#define Point2_h

#ifdef __cplusplus
extern "C" {
#endif

typedef struct Point2
{
	int x;
	int y;
} Point2;

Point2 Point2_new(int x, int y);
Point2 Point2_add(Point2 pointA, Point2 pointB);
Point2 Point2_sub(Point2 pointA, Point2 pointB);
Point2 Point2_mul(Point2 pointA, Point2 pointB);

#ifdef __cplusplus
}
#endif
#endif // !Point2_h

