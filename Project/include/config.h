#ifndef config_h
#define config_h

#if defined(WIN32) || defined(_WIN32)
	#define PLATFORM_WIN32 1
	#if defined(WIN64) || defined(_WIN64)
		#define PLATFORM_WIN64 1
	#endif // WIN64 || _WIN64
#elif  defined(__unix__) || defined(__unix)
	#define PLATFORM_UNIX 1
#elif defined(__linux__) || defined(__linux)
	#define PLATFORM_LINUX 1
#elif defined(__APPLE__) || defined(__APPLE)
	#define PLATFORM_APPLE 1
#endif

#endif // !config_h