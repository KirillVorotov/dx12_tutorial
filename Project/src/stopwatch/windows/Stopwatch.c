#include "config.h"

#if defined(PLATFORM_WIN32)

#include <stdbool.h>
#include <windows.h>
#include "Stopwatch.h"

static LARGE_INTEGER hires_start_ticks;
static LARGE_INTEGER hires_ticks_per_second;

static bool started = false;

void Stopwatch_Init(void)
{
	if (started)
		return;
	started = true;

	QueryPerformanceFrequency(&hires_ticks_per_second);
	QueryPerformanceCounter(&hires_start_ticks);
}

void Stopwatch_Quit(void)
{
	started = false;
}

double Stopwatch_StartTimeMicroseconds(void)
{
	if (!started)
	{
		Stopwatch_Init();
	}

	return hires_start_ticks.QuadPart * (1000000.0 / hires_ticks_per_second.QuadPart);
}

double Stopwatch_TimeMicroseconds(void)
{
	if (!started)
	{
		Stopwatch_Init();
	}

	LARGE_INTEGER hires_now;

	QueryPerformanceCounter(&hires_now);
	return(hires_now.QuadPart - hires_start_ticks.QuadPart) * (1000000.0 / hires_ticks_per_second.QuadPart);
}

#endif