#include <stdio.h>
#include "config.h"
#include "AppRunner.h"

static int Update(double t, double dt)
{
	printf("Update: %f ms\n", Application_RealtimeSinceStartup());
	if (Application_RealtimeSinceStartup() >= 5000.0)
		Application_Stop();
	return 0;
}

static int FixedUpdate(double t, double dt)
{
	printf("FixedUpdate: %f ms\n", Application_FixedTimeSinceStartup());
	return 0;
}

int main(int argc, char *argv[])
{
	int returnCode = 0;

	returnCode = Application_Init();
	if (returnCode != 0)
		return returnCode;
	returnCode = Application_Run(Update, FixedUpdate);
	return returnCode;
}