#include "Point2.h"

Point2 Point2_new(int x, int y)
{
	Point2 point;
	point.x = x;
	point.y = y;
	return point;
}

Point2 Point2_add(Point2 pointA, Point2 pointB)
{
	Point2 result;
	result.x = pointA.x + pointB.x;
	result.y = pointA.y + pointB.y;
	return result;
}

Point2 Point2_sub(Point2 pointA, Point2 pointB)
{
	Point2 result;
	result.x = pointA.x - pointB.x;
	result.y = pointA.y - pointB.y;
	return result;
}

Point2 Point2_mul(Point2 pointA, Point2 pointB)
{
	Point2 result;
	result.x = pointA.x * pointB.x;
	result.y = pointA.y * pointB.y;
	return result;
}