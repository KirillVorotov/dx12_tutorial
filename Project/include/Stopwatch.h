#ifndef Stopwatch_h
#define Stopwatch_h

#ifdef __cplusplus
extern "C" {
#endif

void Stopwatch_Init(void);
void Stopwatch_Quit(void);
double Stopwatch_StartTimeMicroseconds(void);
double Stopwatch_TimeMicroseconds(void);

#ifdef __cplusplus
}
#endif

#endif // !Stopwatch_h