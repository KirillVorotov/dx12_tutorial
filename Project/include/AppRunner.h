#ifndef AppRunner_h
#define AppRunner_h

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/

	static bool initialized = false;
	static bool running = false;

	static double realtimeSinceStartup;
	static double maxFrameTime;
	static double deltaTime;
	static double timeAccumulator;
	static double fixedTime;
	static double fixedDeltaTime;
	static double fixedTimeAccumulator;

	int Application_Init(void);
	int Application_Run(int (*update_callback)(double, double), int (*fixed_update_callback)(double, double));
	void Application_Stop(void);
	double Application_RealtimeSinceStartup(void);
	double Application_DeltaTime(void);
	double Application_FixedTimeSinceStartup(void);
	double Application_FixedDeltaTime(void);

#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif /*!AppRunner_h*/

